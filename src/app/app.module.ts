import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfigService } from './config/config.service';
import { AuthModule } from './features/auth/auth.module';
import { AuthInterceptorService } from './features/auth/interceptor/auth-interceptor.service';
import { CrudModule } from './features/crud/crud.module';
import { PGrowlModule } from './features/p-growl/p-growl.module';
import { AppLayoutModule } from './layout/app-layout/app-layout.module';
import { LayoutModule } from './layout/layout.module';

export function ConfigLoader(configService: ConfigService) {
return () => configService.load();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    CrudModule,
    AppLayoutModule,
    AuthModule,
    PGrowlModule
  ],
  providers: [
     {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi:true
    },
    {
    provide: APP_INITIALIZER, useFactory: ConfigLoader, deps: [ConfigService], multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
