import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CenterComponent } from './center/center.component';

@NgModule({
  declarations: [
    CenterComponent
  ],
  imports: [
    CommonModule, RouterModule
  ], exports: [CenterComponent]
})
export class LayoutModule { }
