import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './features/auth/guards/auth.guard';
import { RoleGuard } from './features/auth/guards/role.guard';
import { LoginComponent } from './features/auth/login/login.component';
import { SignUpComponent } from './features/auth/sign-up/sign-up.component';
import { CenterComponent } from './layout/center/center.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: CenterComponent, children: [{path: '', component: LoginComponent}]},
  {path: 'sign-up', component: CenterComponent, children: [{path: '', component: SignUpComponent}]},
  {path: 'offensive-word',canActivate: [AuthGuard, RoleGuard], loadChildren: () => import('./features/crud/crud.module').then(m=> m.CrudModule)},
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }