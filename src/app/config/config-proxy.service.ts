import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { Config } from './config';

@Injectable({
  providedIn: 'root'
})
export class ConfigProxyService {
    constructor(private httpClient: HttpClient) { }

getConfig(): Observable<Config> {
return this.httpClient.get<Config>(`${environment.configFile}`);
}
}