import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { Config } from './config';

const CONFIG_FAKE: Config = {
api: 'https://local-apps.kairosds.com'
};

@Injectable()
export class ConfigProxyServiceFake {

constructor() { }

getConfig(): Observable<Config> {
return of(CONFIG_FAKE);
}

}
