import { TestBed } from '@angular/core/testing';

import { CrudProxy } from './crud-proxy.service';

describe('CrudProxy.Service.TsService', () => {
  let service: CrudProxy.Service.TsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudProxy.Service.TsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
