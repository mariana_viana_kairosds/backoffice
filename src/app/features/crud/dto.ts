export interface DTO {
    id?: string;
    word?: string;
    level?: number;
}