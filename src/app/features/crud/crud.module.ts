import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CrudComponent } from './crud/crud.component';

const ROUTES : Routes = [{
path: '', component: CrudComponent}];

@NgModule({
  declarations: [
    CrudComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
     RouterModule.forChild(ROUTES)
  ],
  exports: [CrudComponent]
})
export class CrudModule { }
