import { TestBed } from '@angular/core/testing';

import { CrudStoreService } from './crud-store.service';

describe('CrudStoreService', () => {
  let service: CrudStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
