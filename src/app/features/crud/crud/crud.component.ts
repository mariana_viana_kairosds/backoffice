import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { CrudStoreService } from '../crud-store.service';
import { DTO } from '../dto';
import { Model } from '../model';

export enum ActionType {
  CREATE = "send",
  UPDATE = 'Update'
}

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})

export class CrudComponent implements OnInit {
  form!: FormGroup;
  offensiveWord$!: Observable<Model[]>;
  mode!: ActionType;

  constructor(private offensiveWordStore: CrudStoreService ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      id: new FormControl(),
      word: new FormControl(''),
      level: new FormControl(''),
    });
    this.offensiveWordStore.init();
    this.offensiveWord$ = this.offensiveWordStore.get$();
    this.mode = ActionType.CREATE;
  }

  async onSubmit(){
    const newOffensiveWord = {
      word: this.form.get('word')?.value,
      level: this.form.get('level')?.value
    };
    const editOffensiveWord ={
      id: this.form.get('id')?.value,
      word: this.form.get('word')?.value,
      level: this.form.get('level')?.value
    };
     switch (this.mode) {
      case ActionType.CREATE:
        await this.offensiveWordStore.createOffensiveWord(newOffensiveWord);
        this.form.reset();
        break;
      case ActionType.UPDATE:
        await this.offensiveWordStore.updateOffensiveWord(editOffensiveWord);
        this.setCreateMode();
        break;
      default:
        break;
    }
  }

   async updateOffensiveWord(offensiveWord: DTO){
    this.mode = ActionType.UPDATE;
    this.form.get('id')?.setValue(offensiveWord.id);
    this.form.get('word')?.setValue(offensiveWord.word);
    this.form.get('level')?.setValue(offensiveWord.level);
  }

  async deleteOffensiveWord(offensiveWordId: string){
    await this.offensiveWordStore.deleteOffensiveWord(offensiveWordId);
    this.setCreateMode();
  }

    setCreateMode(){
    this.mode = ActionType.CREATE;
    this.form.reset();
  }

}
