import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { CrudProxyService } from './crud-proxy.service';
import { DTO } from './dto';
import { Model } from './model';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private respository: CrudProxyService) { }

  getOffensiveWord(): Observable<Model[]>{
     return this.respository.getOffensiveWord().pipe(
      map((offensive_word: DTO[])=>{
        return offensive_word.map(offensive => {
          return {
            id: offensive.id,
            word:offensive.word,
            level: offensive.level
          }
        })
      })
     )
  }

  addOffensiveWord(offensive_word: Model): Observable<Model> {
    const offensiveWordDto: DTO = {
      word: offensive_word.word!,
      level: offensive_word.level!
    };

    return this.respository.addOffensiveWord(offensiveWordDto).pipe(map(this.dtoToModel))
  }

  updateOffensiveWord(offensiveWord: Model): Observable<Model> {
    const offensiveDTO: DTO = {
      id: offensiveWord.id!,
      word: offensiveWord.word!,
      level: offensiveWord.level!
    };

    return this.respository.updateOffensiveWord(offensiveDTO).pipe(map(this.dtoToModel))
  }

  deleteOffensiveWord(id: string){
    return this.respository.deleteOffensiveWord(id);
  }

  dtoToModel(offensiveWord: DTO): Model {
    return {
      id: offensiveWord.id,
      word: offensiveWord.word,
      level: offensiveWord.level
    };
  }
}
