import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from 'src/app/config/config.service';
import { DTO } from './dto';

@Injectable({
  providedIn: 'root'
})

export class CrudProxyService{

  constructor(private http: HttpClient, private env: ConfigService) { }

  getOffensiveWord(): Observable<DTO[]> {
    return this.http.get<DTO[]>(`${this.env.config.api}/api/offensive-words`)
  }

  addOffensiveWord (offensive_word: DTO): Observable<DTO>{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.http.post<DTO>(`${this.env.config.api}/api/offensive-word`, offensive_word, {headers});
  };

  updateOffensiveWord(offensiveWord: DTO): Observable<DTO> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.http.patch<DTO>(`${this.env.config.api}/api/offensive-word/${offensiveWord.id}`, offensiveWord, {headers});
  }
  
  deleteOffensiveWord(id: string): Observable<Object>{
    return this.http.delete<Object>(`${this.env.config.api}/api/offensive-word/${id}`)
  }
}
