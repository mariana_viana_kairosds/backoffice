import { Injectable } from '@angular/core';
import { lastValueFrom, tap } from 'rxjs';
import { Store } from '../../state/store';
import { CrudService } from './crud-service.service';
import { Model } from './model';

@Injectable({
  providedIn: 'root',
})
export class CrudStoreService extends Store<Model[]> {
  constructor(private service: CrudService) {
    super();
  }

  init() {
    lastValueFrom(
      this.service
        .getOffensiveWord()
        .pipe(tap((offensiveWord) => this.store(offensiveWord)))
    );
  }

  createOffensiveWord(offensiveWord: Model) {
    return lastValueFrom(
      this.service
        .addOffensiveWord(offensiveWord)
        .pipe(
          tap((createOffensiveWord) =>
            this.store([...this.get(), createOffensiveWord])
          )
        )
    );
  }

  updateOffensiveWord(offensiveWord: Model) {
    return lastValueFrom(
      this.service
        .updateOffensiveWord(offensiveWord)
        .pipe(
          tap((updateOffensiveWord) =>
            this.store(
              this.get().map((storeOffensiveWord) =>
                storeOffensiveWord.id === updateOffensiveWord.id
                  ? updateOffensiveWord
                  : storeOffensiveWord
              )
            )
          )
        )
    );
  }

  deleteOffensiveWord(OffensiveWordId: string) {
    return lastValueFrom(
      this.service
        .deleteOffensiveWord(OffensiveWordId)
        .pipe(
          tap(() =>
            this.store(
              this.get().filter((store) => store.id !== OffensiveWordId)
            )
          )
        )
    );
  }
}
