export interface Model{
    id?: string;
    word?: string;
    level?: number;
}