import { Component, OnInit } from '@angular/core';
import { NotificacionsBusService } from '../../auth/services/notifications-bus.service';

@Component({
  selector: 'app-growl',
  templateUrl: './growl.component.html',
  styleUrls: ['./growl.component.css']
})
export class GrowlComponent implements OnInit {

  msgs: Notification[] = [];

  constructor(private notificacionBus: NotificacionsBusService) { }

  ngOnInit(): void {
   this.notificacionBus.getNotificacion();
  }

/*.subscribe(
      (notificacion: any) => {
        this.msgs = [];
        this.msgs.push(notificacion);
      }
    )*/

}
