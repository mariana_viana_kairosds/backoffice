import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GrowlComponent } from './growl/growl.component';



@NgModule({
  declarations: [
    GrowlComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    GrowlComponent
  ]
})
export class PGrowlModule { }
