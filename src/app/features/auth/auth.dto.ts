export interface AuthDTO {
    token: string
}

export interface CredentialsDTO {
    email: string;
    password: string
}

export interface Role {
    role: string
}

export interface AuthCredentialsDTO {
    nickName: string;
    email: string;
    password: string;
    role: string;
}