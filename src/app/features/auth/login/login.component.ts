import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthLoginModel } from '../auth.model';
import { AuthService } from '../services/auth.service';
import { RoleStoreService } from '../services/role-store.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form!: FormGroup;

  constructor(
    private authService: AuthService,
    private authStore: RoleStoreService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

 onSubmit() {
    const authCredentials: AuthLoginModel = {
      email: this.form.get('email')?.value,
      password: this.form.get('password')?.value,
    };

    this.authService.login(authCredentials).subscribe({
      next:(resp)=>{localStorage.setItem('token', resp.token);
    },
    complete:()=> this.router.navigate(['/offensive-word'])
    })
  }
}
