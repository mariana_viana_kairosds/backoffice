import { TestBed, waitForAsync } from '@angular/core/testing';
import { NotificacionsBusService } from './notifications-bus.service';
 
describe('Notificaciones bus service', () => {
 
    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            providers: [NotificacionsBusService]
        });
    }));
 
    it('should enviar y obtener una notificacion', () => {
        const msg = 'Prueba';
        const service: NotificacionsBusService = TestBed.inject(NotificacionsBusService);
        service.getNotificacion().subscribe({
           next: (noti) => {
            console.log(noti.detail);
            expect(noti.detail).toEqual(msg);
           }
          });
        service.showError(msg, '2');
        service.showWarn(msg, '2');
        service.showInfo(msg, '2');
    });
});


/*noti => {
                console.log(noti.detail);
                expect(noti.detail).toEqual(msg);
            },
            error => {
                fail(error);
            }*/