import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AuthDTO } from '../auth.dto';
import { AuthTokenModel } from '../auth.model';


@Injectable({
  providedIn: 'root'
})
export class AuthProxyServiceFake {

  constructor() { }

 login(email: string, password: string): Observable<AuthTokenModel>{
   const authToken: AuthDTO = {
    token: 'xxxx'
   }
   return of(authToken);
  }
}
