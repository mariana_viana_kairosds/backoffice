import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';
import { Store } from 'src/app/state/store';

@Injectable({
  providedIn: 'root'
})
export class LoggedService extends Store<boolean> {

  constructor() { 
    super();
    this.init();

    fromEvent(window, 'beforeunload').subscribe(()=> {
      this.save()
    })
    this.load();
  }

  init(){
    this.store(false);
  }

  loginIn(){
    this.store(true);
    this.save();
  }

  logOut() {
    this.store(false);
    this.save();
  }

  save() {
    const state = this.get();
    localStorage.setItem('LOGGED_STATE', JSON.stringify(state));
    this.store(state);
  }

  load() {
    const loggedState = localStorage.getItem('LOGGED_STATE');
    if (loggedState) {
      const state = JSON.parse(loggedState);
      this.store(state);
      this.store(state);
      localStorage.removeItem('LOGGED_STATE');
    }
  }
}
