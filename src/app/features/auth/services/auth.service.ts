import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthCredentialsDTO, CredentialsDTO, Role } from '../auth.dto';
import { AuthCredentialsModel, AuthTokenModel } from '../auth.model';
import { AuthProxyService } from './auth-proxy.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private repository: AuthProxyService) { }

  login(credentials: CredentialsDTO): Observable<AuthTokenModel>{
  return this.repository.login(credentials);
  }

  signUp(auth: AuthCredentialsModel): void{
    const signAuth : AuthCredentialsDTO = {
      nickName: auth.nickName,
      email: auth.email,
      password: auth.password,
      role: auth.role
    };
    this.repository.signUp(signAuth).subscribe(()=>this.dtoToModel)
  }

  getRole(): Observable<Role>{
   return this.repository.getRole()
  }

  dtoToModel(auth: AuthCredentialsDTO): AuthCredentialsModel {
    return{
      nickName: auth.nickName,
      email: auth.email,
      password:auth.password,
      role: auth.role
    };
  }

}
