import { Injectable } from '@angular/core';
import { lastValueFrom, tap } from 'rxjs';
import { Store } from 'src/app/state/store';
import { AuthRoleModel } from '../auth.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleStoreService extends Store<AuthRoleModel> {

  constructor(private service: AuthService) {
    super();
   }

   getRole(){
    return lastValueFrom(
      this.service.getRole().pipe(tap((role)=> this.store(role)))
     )
   }

}
