import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { Notification } from './notification';

@Injectable({
  providedIn: 'root',
})
export class NotificacionsBusService {
  shownotificacionSource!: ReplaySubject<Notification>;

  constructor() {}

  getNotificacion(): Observable<Notification> {
    return this.shownotificacionSource;
  }

  showError(msg: string, summary: string) {
    this.show('error', summary, msg);
  }

  showInfo(msg: string, summary: string) {
    this.show('info', summary, msg);
  }

  showWarn(msg: string, summary: string) {
    this.show('warn', summary, msg);
  }

  private show(severity: string, summary: string, msg: string) {
    const notificacion : Notification = {
       severity: severity,
       summary: summary,
       detail: msg
    }
    this.notify(notificacion);
  }

  private notify(notificacion: Notification): void {
    this.shownotificacionSource.next(notificacion);
  }
}
