import { HttpTestingController } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { AuthDTO, CredentialsDTO } from '../auth.dto';
import { AuthProxyService } from './auth-proxy.service';

describe('AuthProxyService', () => {
  let service: AuthProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call login with credentials', waitForAsync(()=>{
    const credentials: CredentialsDTO = {
      email: 'prueba@gmail.com',
      password: 'hello'
    }
    const httpMock = TestBed.inject(HttpTestingController);
    service.login(credentials).subscribe(
      authToken => {
        expect(authToken.token).toEqual(FAKE_AUTH_TOKEN.token)
      }
    );
    const request = httpMock.expectOne('http://localhost:3001/login');
    expect(request.request.method).toEqual('POST');
    request.flush(FAKE_AUTH_TOKEN);
    httpMock.verify();
  }))
});

const FAKE_AUTH_TOKEN: AuthDTO = {
  token: 'xxxx'
};

