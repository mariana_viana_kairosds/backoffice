import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from 'src/app/config/config.service';
import { CredentialsDTO, Role } from '../auth.dto';
import { AuthTokenModel } from '../auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthProxyService {

  constructor(private http: HttpClient, private env: ConfigService) { }

  login(credentials: CredentialsDTO): Observable<AuthTokenModel>{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.http.post<AuthTokenModel>(`${this.env.config.api}/api/login`,credentials, {headers});
  }

 signUp(credentials: CredentialsDTO): Observable<CredentialsDTO>{
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('Content-type', 'application/json; charset=UTF-8');
    return this.http.post<CredentialsDTO>(`${this.env.config.api}/api/sign-up`, credentials, {headers});
  }

  getRole(): Observable<Role>{
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.set('Content-type', 'application/json; charset=UTF-8');
      return this.http.get<Role>(`${this.env.config.api}/api/auth/role/me`, {headers});
  }
}
