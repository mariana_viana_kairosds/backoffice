import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { RoleStoreService } from '../services/role-store.service';



@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private store: RoleStoreService){}
  
  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    await this.store.getRole();
    const role = this.store.get()?.role;
   if(role === 'ADMIN'){
    return true;
   }else{
    return false;
   }
  }
  
}
