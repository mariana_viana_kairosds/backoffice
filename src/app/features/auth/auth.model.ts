export interface AuthCredentialsModel{
    nickName: string;
    email: string;
    password: string;
    role: string;
}

export interface AuthLoginModel {
    email: string;
    password: string;
}

export interface AuthTokenModel{
    token: string
}

export interface AuthRoleModel{
    role: string
}