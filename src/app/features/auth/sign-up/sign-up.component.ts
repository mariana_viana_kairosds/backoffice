import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthCredentialsModel } from '../auth.model';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  form!: FormGroup;

  constructor(private authService: AuthService, private route: Router) { }

  ngOnInit(): void {
     this.form = new FormGroup({
      nickName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      role: new FormControl('Select Role', [Validators.required])
    });
  }

  onSubmit(){
    const authCredentials: AuthCredentialsModel = {
      nickName: this.form.get('nickName')?.value,
      email: this.form.get('email')?.value,
      password: this.form.get('password')?.value,
      role: this.form.get('role')?.value
    };
    this.authService.signUp(authCredentials);
    this.route.navigate(['/login'])
  }
}
