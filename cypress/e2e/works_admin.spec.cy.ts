it('login works with admin user and offensive words', () => {
  cy.visit('/login');
  cy.get('#email').type('admin@gmail.com');
  cy.get('#pass').type('admin');
  cy.get('button').click();
});

it('go offensive words and add word', ()=>{
  cy.get('[type="text"]').type('caca');
  cy.get('[type="number"]').type('2');
  cy.get('.form-search > button').click();  
  
})

it('change and delete a word', () => {
  cy.get('.container > :nth-child(1) > :nth-child(4) > :nth-child(1)').click();
  cy.get('[type="number"]').clear().type('1'); 
  cy.get('[type="submit"]').click();
  cy.get('.container > :nth-child(1) > :nth-child(4) > :nth-child(2)').click();
})

